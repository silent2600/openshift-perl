#!/usr/bin/perl
use strict;
use warnings;

#  printenv -- demo CGI program which just prints its environment
print "Content-type: text/html\n\n";

my $count;
$count = &inc_number();
print "pid = $$ count = $count<br/>\n\n";

delete $ENV{SERVER_SIGNATURE};

foreach my $key ( sort( keys(%ENV) ) ) {
    next if $key =~/^OPENSHIFT/;
    my $val = $ENV{$key};
    $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
    print qq~$key = $val\n<br/>~;
}

sub inc_number {
    $count++;
}

exit();

